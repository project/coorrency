<?php

namespace Drupal\coorrency\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Client as GuzzleClient;
use Drupal\Core\Url;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class HttpClient.
 *
 * @package Drupal\coorrency\Service
 */
class HttpClient {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Cache Backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Base uri.
   */
  protected $apiBaseUri;

  /**
   * Api key
   */
  protected $apiKey;

  /**
   * Guzzle.
   */
  protected $guzzleClient;

  /**
   * HttpClient constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   Config.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   LoggerChannelFactory.
   */
  public function __construct(ConfigFactory $config, CacheBackendInterface $cacheBackend, LoggerChannelFactoryInterface $loggerFactory) {
    // Get the config object.
    $this->config = $config->get('coorrency.settings');
    // Set cache Backend.
    $this->cacheBackend = $cacheBackend;
    // Set logger.
    $this->loggerFactory = $loggerFactory;

    // Get config from database.
    $this->setCurrencyConfig();

    // Create the Guzzle client with base_uri parameter to apply to
    // each request.
    $this->guzzleClient = new GuzzleClient([
      'base_uri' => $this->apiBaseUri,
    ]);
  }

  /**
   * getCurrencyConfig
   */
  private function setCurrencyConfig() {
    // Get server to connect.
    $api_server = $this->config->get('coorrency.api_server');
    if ($api_server == 'dedicated') {
      $api_server = $this->config->get('coorrency.custom_domain');
    }
    // Form base_uri to the request, this request use API v7.
    // See https://www.currencyconverterapi.com/change-log
    $this->apiBaseUri = 'https://' . $api_server . '.currconv.com/api/v7';

    // Get API Key.
    $this->apiKey = $this->config->get('coorrency.api_key');
  }

  /**
   * checkAPIConection @TODO Review this.
   */
  public function checkAPIConection() {
    $message = '';
    try {
      $response = $this->guzzleClient->request('GET', $this->apiBaseUri);
    } catch (\Exception $error) {
      $message = $error->getMessage();
    }
    return $message;
  }

  /**
   * Coorrency request.
   *
   * @param string $relative_url
   *   Relative url to add to base_uri.
   * @param array $args
   *   Args to request.
   * @param bool $cacheable
   *   Is it cachable.
   *
   * @return bool|array
   *   Either an array or false.
   */
  public function request(string $relative_url = '', array $queries = [], $cacheable = FALSE) {
    // Reset config from database.
    $this->setCurrencyConfig();

    // Update base_uri.
    $this->apiBaseUri = $this->apiBaseUri . $relative_url;

    // Set default request queries for the request.
    $queries = $this->buildQueries($queries);
    // Build hash for caching.
    $cid = $this->buildArgHash($queries);

    // Check cache.
    if ($cache = $this->cacheBackend->get($cid)) {
      $response = $cache->data;
      // Return result from cache if found.
      return $response;
    }
    else {
      $response = $this->doRequest($queries, $relative_url);
      if ($response) {
        // Cache the response if we got one.
        if ($cacheable) {
          // @TODO Cache never expire unless it is deleted explicitly,
          // review it for set an expire time.
          $this->cacheBackend->set($cid, $response);
        }
        // Return result from source if found.
        return $response;
      }
    }
    // No results.
    return FALSE;
  }

  /**
   * Associative array of query string values or query string to add
   * to the request.
   *
   * @param array $queries
   *   Queries to add to the request.
   *
   * @return array
   *   Return the queries array.
   */
  private function buildQueries(array $queries) {
    $queries['apiKey'] = $this->apiKey;
    return $queries;
  }

  /**
   * Build Hash from Args array.
   *
   * This is uses for Cache cid in the format:
   * coorrency_api:1f3870be274f6c49b3e31a0c6728957f
   *
   * @param array $args
   *   Args to request.
   *
   * @return string
   *   Return string.
   */
  private function buildArgHash(array $args) {
    $argHash = '';
    foreach ($args as $key => $value) {
      $argHash .= $key . $value;
    }
    return 'coorrency_api:' . md5($argHash);
  }

  /**
   * Guzzle request for Coorrency.
   *
   * @param string $url
   *   Url.
   * @param array $parameters
   *   Parameters.
   * @param string $requestMethod
   *   Request method.
   *
   * @return bool|array
   *   False or array.
   */
  private function doRequest(array $queries, $relative_url) {
    try {
      // Set method GET by default for coorrency request.
      $response = $this->guzzleClient->request('GET', $this->apiBaseUri, [
        'query' => $queries
      ]);

      if ($response->getStatusCode() == 200) {
        $contents = $response->getBody()->getContents();
        $result = Json::decode($contents);

        switch ($relative_url) {
          case '/currencies':
            if (isset($result['results'])) {
              $currencies = array_column($result['results'], 'id', 'id');
              asort($currencies);
              return $currencies;
            }
            break;
          case '/convert':
            if (count($result) > 0) {
              $val = floatval($result[$queries['q']]);
              $total = $val * $queries['amount'];
              return number_format($total, 2, '.', '');
            }
            break;
        }
      }
      else {
        return FALSE;
      }
    }
    catch (GuzzleException $error) {
      $this->loggerFactory->get('coorrency_api')->error("@message", [
        '@message' => $error->getMessage()
      ]);
      return FALSE;
    }
  }

}
