<?php

namespace Drupal\coorrency\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block plugin definitions for custom coorrency blocks list.
 *
 * @see \Drupal\coorrency\Plugin\Block\CoorrencyBlockList
 */
class CoorrencyBlockList extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The currency exchange storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $currencyStorage;

  /**
   * Constructs new CoorrencyBlockList.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $currency_storage
   *   The currency exchange storage.
   */
  public function __construct(EntityStorageInterface $currency_storage) {
    $this->currencyStorage = $currency_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')->getStorage('currency_exchange')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->currencyStorage->loadMultiple() as $currency => $entity) {
      $this->derivatives[$currency] = $base_plugin_definition;
      $this->derivatives[$currency]['admin_label'] = $entity->label();
      $this->derivatives[$currency]['config_dependencies']['config'] = [$entity->getConfigDependencyName()];
    }
    return $this->derivatives;
  }

}
