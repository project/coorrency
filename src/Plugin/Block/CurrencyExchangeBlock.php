<?php

namespace Drupal\coorrency\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a Currency Exchange Block in which you can convert values anywhere.
 *
 * @Block(
 *   id = "currency_exchange_block",
 *   admin_label = @Translation("Currency Exchange"),
 * )
 */
class CurrencyExchangeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return \Drupal::formBuilder()->getForm('Drupal\coorrency\Form\CurrencyExchangeForm');
  }

}
