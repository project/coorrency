<?php

namespace Drupal\coorrency\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\coorrency\Service\HttpClient;

/**
 * Provides a generic Coorrency block list.
 *
 * @Block(
 *   id = "coorrency_block_list",
 *   admin_label = @Translation("Coorrency block list"),
 *   category = @Translation("Coorrency blocks list"),
 *   deriver = "Drupal\coorrency\Plugin\Derivative\CoorrencyBlockList",
 * )
 */
class CoorrencyBlockList extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The pool configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * HttpClient.
   */
  protected $httpClient;

  /**
   * Constructs a new SystemMenuBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, HttpClient $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('coorrency.settings');
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('coorrency.http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // @TODO Improve.
    $response = $this->httpClient->checkAPIConection();

    if (!empty($response)) {
      $this->messenger()->addError($response);
      return [
        '#markup' => 'No API conection.'
      ];
    }

    $config_id = $this->getDerivativeId();
    $entity_config = $this->entityTypeManager->getStorage('currency_exchange')->load($config_id);

    $cant = $entity_config->getCant();
    $from_currency = $entity_config->getCurrencyFrom();
    $to_coins = $entity_config->getCurrencyTo();

    $get_changes = [];
    foreach ($to_coins as $to_currency) {
      $result = $this->convertCurrency($cant, $from_currency, $to_currency);
      $get_changes[] = "$cant $from_currency = $result $to_currency";
    }

    $build['theme_currency'] = [
      '#theme' => 'item_list',
      '#items' => $get_changes,
    ];

    return $build;
  }

  /**
   * Constructs a new SystemMenuBlock.
   *
   * @param string $amount
   *   The input amount.
   * @param string $from_currency
   *   The input currency code.
   * @param string $to_currency
   *   The output currency code.
   *
   * @return string|float
   *   The converted amount.
   */
  public function convertCurrency($amount, $from_currency, $to_currency) {
    $queries = [
      'q' => urlencode($from_currency) . '_' . urlencode($to_currency),
      'compact' => 'ultra',
      'amount' => $amount
    ];

    $value = $this->httpClient->request('/convert', $queries);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // Don't cache this block because it's necessary to have the most recent
    // update of the exchange rate.
    return 0;
  }

}
