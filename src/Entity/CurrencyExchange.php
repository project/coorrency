<?php

namespace Drupal\coorrency\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Currency exchange entity.
 *
 * @ConfigEntityType(
 *   id = "currency_exchange",
 *   label = @Translation("Currency exchange"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\coorrency\Entity\CurrencyExchangeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\coorrency\Form\CurrencyExchangeListForm",
 *       "edit" = "Drupal\coorrency\Form\CurrencyExchangeListForm",
 *       "delete" = "Drupal\coorrency\Form\CurrencyExchangeListDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\coorrency\Entity\CurrencyExchangeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "currency_exchange",
 *   admin_permission = "administer currency",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/currency_exchange/{currency_exchange}",
 *     "add-form" = "/admin/structure/currency_exchange/add",
 *     "edit-form" = "/admin/structure/currency_exchange/{currency_exchange}/edit",
 *     "delete-form" = "/admin/structure/currency_exchange/{currency_exchange}/delete",
 *     "collection" = "/admin/structure/currency_exchange"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "cant",
 *     "currency_from",
 *     "currency_to",
 *   },
 * )
 */
class CurrencyExchange extends ConfigEntityBase implements CurrencyExchangeInterface {

  /**
   * The Currency exchange ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Currency exchange label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Currency exchange label.
   *
   * @var string
   */
  protected $cant;

  /**
   * The Currency exchange label.
   *
   * @var string
   */
  protected $currency_from;

  /**
   * The Currency exchange label.
   *
   * @var string
   */
  protected $currency_to = [];

  /**
   * Get cant.
   */
  public function getCant() {
    if (empty($this->cant)) {
      return 1;
    }
    return $this->cant;
  }

  /**
   * Get currency from.
   */
  public function getCurrencyFrom() {
    return $this->currency_from;
  }

  /**
   * Get currency to.
   */
  public function getCurrencyTo() {
    return $this->currency_to;
  }

}
