<?php

namespace Drupal\coorrency\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Currency exchange entities.
 */
interface CurrencyExchangeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
