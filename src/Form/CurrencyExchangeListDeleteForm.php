<?php

namespace Drupal\coorrency\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\block\Entity\Block;

/**
 * Builds the form to delete Currency exchange entities.
 */
class CurrencyExchangeListDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', [
      '%name' => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('The currency exchange block associated with this entity will also be removed.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.currency_exchange.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Delete block associated.
    $block = Block::load($this->entity->id());
    if ($block) {
      $block->delete();
    }
    // Delete current CurrencyExchange entity.
    $this->entity->delete();

    $this->messenger()
      ->addMessage($this->t('The entity "@label" was removed.', [
        '@label' => $this->entity->label(),
      ]));
    $this->messenger()->addMessage($this->t('The associated Block was also removed.'));

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
