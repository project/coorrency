<?php

namespace Drupal\coorrency\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\coorrency\Entity\CurrencyExchange;

/**
 * CurrencyExchangeForm Class.
 */
class CurrencyExchangeForm extends FormBase {

  /**
   * configCurrencies.
   */
  protected $configCurrencies;

  /**
   * getAllCurrencies.
   */
  protected $allCurrencies;

  /**
   * Constructs a CurrencyExchangeForm object.
   */
  public function __construct() {
    $this->configCurrencies = $this->config('coorrency.settings');
    $this->allCurrencies = \Drupal::service('coorrency.http_client')->request('/currencies');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'currency_exchange_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // @TODO Improve.
    $result = \Drupal::service('coorrency.http_client')->checkAPIConection();
    $this->messenger()->addError($result);

    if (!$this->allCurrencies) {
      return [
        '#markup' => 'No API conection.'
      ];
    }

    $form['amount'] = [
      '#type' => 'number',
      '#min' => 1,
      '#required' => TRUE,
      '#maxlength' => 50,
      '#size' => 100,
    ];

    $form['from'] = [
      '#type' => 'select',
      '#options' => $this->allCurrencies,
      '#required' => TRUE,
      '#attached' => [
        'library' => [
          'coorrency/coorrency'
        ],
        // Send API data to js.
/*        'drupalSettings' => [
          'coorrency' => [
            'api_key' => $this->configCurrencies->get('coorrency.api_key'),
            'api_server' => $this->configCurrencies->get('coorrency.api_server'),
            'custom_domain' => $this->configCurrencies->get('coorrency.custom_domain'),
          ]
        ]*/
      ]
    ];

    // Add swap button to let exchange currencies.
    $use_swap = $this->configCurrencies->get('coorrency.swap');
    if ($use_swap) {
      $form['swap'] = [
        '#markup' => '<span class="coorrency-swap">⇅</span>',
      ];
    }

    $form['to'] = [
      '#type' => 'select',
      '#title' => $this->t('To'),
      '#options' => $this->allCurrencies,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Convert'),
      '#ajax' => [
        'callback' => [$this, 'getRequestValue'],
        'wrapper' => 'convert-result',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Converting...'),
        ],
      ]
    ];

    $form['coorrency'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => [
        'id' => 'convert-result'
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Callback for submit_driven example.
   *
   * Select the 'box' element, change the markup in it, and return it as a
   * renderable array.
   *
   * @return array
   *   Renderable array (the box element)
   */
  public function getRequestValue(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $queries = [
      'q' => urlencode($values['from']) . '_' . urlencode($values['to']),
      'compact' => 'ultra',
      'amount' => $values['amount']
    ];

    $value = \Drupal::service('coorrency.http_client')->request('/convert', $queries);
    $value = 63;
    $form['coorrency']['#value'] = $value;

    return $form['coorrency'];
  }

}
