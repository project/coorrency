<?php

namespace Drupal\coorrency\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CurrencyExchangeForm.
 */
class CurrencyExchangeListForm extends EntityForm {

  /**
   * getAllCurrencies.
   */
  protected $allCurrencies;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $this->allCurrencies = \Drupal::service('coorrency.http_client')->request('/currencies');
    if (!$this->allCurrencies) {
      $this->allCurrencies = ['USD' => 'USD'];
    }

    $form['cant'] = [
      '#type' => 'number',
      '#title' => $this->t('Convert'),
      '#size' => 100,
      '#maxlength' => 50,
      '#min' => 1,
      '#max' => 100,
      '#required' => TRUE,
      '#default_value' => $this->entity->getCant(),
    ];

    $form['currency_from'] = [
      '#type' => 'select',
      '#title' => $this->t('From'),
      '#options' => $this->allCurrencies,
      '#required' => TRUE,
      '#default_value' => $this->entity->getCurrencyFrom(),
    ];

    $form['currency_to'] = [
      '#type' => 'select',
      '#title' => $this->t('To'),
      '#options' => $this->allCurrencies,
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#size' => 10,
      '#default_value' => $this->entity->getCurrencyTo(),
      '#description' => $this->t('The conversion to the selected currencies is displayed.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    !is_null($this->entity->id()) ? $action = $this->t('Update'):
    $action = $this->t('Create');

    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('@action Currency exchange', ['@action' => $action]),
      '#submit' => ['::submitForm', '::save'],
    ];
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $currency_to = $form_state->getValue('currency_to');
    if (count($currency_to) > 10) {
      $form_state->setErrorByName('currency_to', $this->t('Select a maximum of 10 currencies to show the conversion'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    if (!$this->entity->id()) {
      $this->entity->set('id', 'currency_exchange_' . time());
    }

    // It allows to clear cache so that the blocks appear once a configuration
    // entity is created in the block list.
    \Drupal::service('plugin.cache_clearer')->clearCachedDefinitions();

    $currency_from = $form_state->getValue('currency_from');
    $this->entity->set('label', $this->t('Current conversion') . " ($currency_from)");

    $status = $this->entity->save();
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Currency exchange.', [
          '%label' => $this->entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Currency exchange.', [
          '%label' => $this->entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

}
