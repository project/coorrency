<?php

namespace Drupal\coorrency\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Currency Setting Form.
 */
class CurrencySettingForm extends ConfigFormBase {

  /**
   * Servers used by API.
   */
  protected $servers = [
    'api'       => 'Premium',
    'prepaid'   => 'Prepaid',
    'free'      => 'Free (for testing)',
    'dedicated' => 'Custom domain'
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'coorrency_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('coorrency.settings');

    $form['api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['api']['server'] = [
      '#type' => 'select',
      '#title' => $this->t('Server'),
      '#options' => $this->servers,
      '#default_value' => $config->get('coorrency.api_server'),
      '#description' => $this->t('The free server is for testing purposes and is subject to downtimes. For production sites consider going for the <a target="_blank" href="https://www.currencyconverterapi.com/docs">paid server</a>.'),
    ];

    $form['api']['custom_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add custom domain'),
      '#default_value' => $config->get('coorrency.custom_domain'),
      '#size' => 20,
      //'#validated' => TRUE,
      '#field_prefix' => 'https://',
      '#field_suffix' => '.currconv.com',
      //'#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="server"]' => ['value' => 'dedicated'],
        ],
      ],
    ];

    $form['api']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add the api key'),
      '#default_value' => $config->get('coorrency.api_key'),
      '#size' => 30,
      '#required' => TRUE,
    ];

    $form['api']['swap'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add the swap button (<span class="coorrency-swap">⇅</span>)'),
      '#default_value' => $config->get('coorrency.swap'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $server = $form_state->getValue('server');
    if ($server != 'dedicated') {
      $form_state->setValue('custom_domain', '');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('coorrency.settings');
    $config->set('coorrency.api_server', $form_state->getValue('server'));
    $config->set('coorrency.custom_domain', $form_state->getValue('custom_domain'));
    $config->set('coorrency.api_key', $form_state->getValue('key'));
    $config->set('coorrency.swap', $form_state->getValue('swap'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'coorrency.settings',
    ];
  }

}
