(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.coorrency = {
    attach: function (context, settings) {

      $('#currency-exchange-form', context).on('submit', function(e) {
        var from = $(this).find('#edit-from').val();
        var to = $(this).find('#edit-to').val();

        if (from == '' || to == '') {
          $('#currency-exchange-form').append('<div class="error">' + Drupal.t('Please fill the form correctly.') + '</div>');
          return false;
        }
        // Get API data.
        var api_key = settings.coorrency.api_key;
        var api_server = settings.coorrency.api_server;
        var custom_domain = settings.coorrency.custom_domain;

        // Set free and paid url.
        var api_url = 'https://free.currconv.com/api/v7/convert';
        if (api_key_version) {
          var api_url = 'https://api.currconv.com/api/v7/convert';
        }
        $.ajax({
          type: 'GET',
          url: api_url + '?q=' + from + '_' + to + '&compact=ultra&apiKey=' + api_key,
          dataType: 'jsonp',
          success: coorrencyJsonParser
        });
        e.preventDefault();
      });

      $('#currency-exchange-form').find('.coorrency-swap').on('click', function() {
        var to = $(this).parent().find('#edit-to').val();

        $(this).parent().find('#edit-to').val($(this).parent().find('#edit-from').val());
        $(this).parent().find('#edit-from').val(to);
      });

      function coorrencyJsonParser(coorrencyJson) {
        var currencyFrom = $('#currency-exchange-form').find('#edit-from').val();
        var currencyTo = $('#currency-exchange-form').find('#edit-to').val();
        var coorrencyRate = Object.values(coorrencyJson);

        if ($('#currency-exchange-form').find('#edit-amount').val() != '') {
          var amount = $('#currency-exchange-form').find('#edit-amount').val().replace(',', '');
          if (!isNaN(amount)) {
            coorrencyRate *= amount;
          }
          else {
            var message = Drupal.t('Please enter a number.');
          }
        }
        if (message) {
          $('#coorrency-rate').html('<div class="coorrency-rate-amount"><div class="error">' + message + '</div></div>');
        }
        else {
          $('#coorrency-rate').html('<div class="coorrency-rate-amount">' + (coorrencyRate * 1).toFixed(2) + ' ' + currencyTo + '</div>');
        }
      }
    }
  };
}(jQuery, Drupal, drupalSettings));
