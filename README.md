Coorrency
===========

Currency converter for Drupal.

Instructions
------------

Unpack in the *modules* folder (currently in the root of your Drupal 8
installation) and enable in `/admin/modules`.

Then, visit `/admin/config/services/coorrency` to configure it.

Last, visit `www.example.com/coorrency/convert/from/to` where:
- *from* is the base currency
- *to* is the second currency for conversion.

* The module also creates a dynamic block that can be placed anywhere.

.......................

* The module also creates dynamic blocks through configuration entities where you
can create custom lists of currency conversions.
 - To add new blocks visit /admin/structure/currency_exchange

Attention
---------

Most bugs have been ironed out, holes covered, features added. But this module
is a work in progress. Please report bugs and suggestions, ok?
